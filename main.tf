provider "aws" {
  region = "eu-central-1"
  access_key = "AKFPOJMDSIFJDSFDPSOGV"
  secret_key = "SAFJKSAPOKSAPODSADKpasdkasfASPofJSAfaspfj"
}

variable "cidr_blocks" {
    description = "cidr block and name tags for apc and subnet"
    type = list(object({
        cidr_block = string
        name = string
    }))
}


# resources

resource "aws_vpc" "developement-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        Name: var.cidr_blocks[0].name
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.developement-vpc.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = "eu-central-1a"
    tags = {
        Name: var.cidr_blocks[1].name
    }
}